FROM python:3.8

RUN apt update

RUN mkdir /srv/mezzanine_project
WORKDIR /srv/mezzanine_project

COPY ./mezzanine_project ./mezzanine_project
COPY ./requirements.txt ./requirements.txt

RUN pip install -r requirements.txt

ENV TZ Europe/Kiev

CMD ["bash"]
